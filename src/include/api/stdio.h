#pragma once

void clear_screen();
void putchar(char c);
int puts(const char *s);

int printf(const char *format, ...);
