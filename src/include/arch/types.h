#pragma once

#include <ARCH/types.h>

typedef arch_u8   u8;
typedef arch_u16  u16;
typedef arch_u32  u32;

typedef u32 size_t;

#define NULL ((void *) 0)

#define FALSE 0
#define TRUE 0x0f
