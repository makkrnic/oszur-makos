#pragma once

#include <ARCH/interrupt.h>
#include <arch/types.h>

void arch_init_interrupts();
void arch_register_interrupt_handler(u32 int_num, void *handler);
void arch_enable_interrupt(u32 irq);
void arch_disable_interrupt(u32 irq);
