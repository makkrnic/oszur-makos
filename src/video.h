#pragma once

#define VIDEO ( (volatile char *) 0x000B8000  ) /* address of video memory  */
#define COLS  80 /* number of characters in a COLS80umn */
#define ROWS  25 /* number of characters in a row */
#define ATTR  7  /* font: white char on black bacground */
