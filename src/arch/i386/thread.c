#include "thread.h"
#include "time.h"
#include <stdio.h>

static char thread_stacks[MAX_THREADS][THREAD_STACK_SIZE] = { { 0 } };
static pthread_t system_threads[MAX_THREADS];
static int current_thread_index;

void arch_create_thread_context(context_t **context, thread_func func, void *param, void (*thread_exit)(), void *stack, size_t stack_size
) {
  u32 *thread_stack = stack + stack_size;
  
  // thread param
  thread_stack--;
  *thread_stack = (u32)param;

  // return address
  thread_stack--;
  *thread_stack = (u32)thread_exit;

  *context = (void *) thread_stack - sizeof(context_t);

  (*context)->eip = (u32) func;
  (*context)->eflags = INIT_EFLAGS;
}

/* switch from one thread to another */
void arch_switch_to_thread(context_t **context_from, context_t **context_to) {
   asm volatile (
      "cmpl   $0, %1     \n\t"   /* is "from" given? */
      "je     1f    \n\t"

      "pushl  $2f        \n\t"   /* EIP */
      "pushfl       \n\t"   /* EFLAGS */
      "pushal       \n\t"   /* all registers */
      "movl   %%esp, %0  \n\t"   /* save stack => from */

   "1:      movl   %2, %%esp  \n\t"   /* restore stack <= to*/
      "popal        \n\t"
      "popfl        \n\t"
      "ret          \n\t"

   "2:      nop          \n\t"

      : "=m" (*context_from)  /* %0 */
      : "m" (context_from),      /* %1 */
        "m" (*context_to)     /* %2 */
   );
}

void pthreads_init() {
  for (int i = 0; i < MAX_THREADS; ++i) {
    system_threads[i].free = TRUE;
    system_threads[i].id = i;
    system_threads[i].stack_address = (void *) &thread_stacks[i];
  }
  current_thread_index = -1;
}

pthread_t *pthread_get_free_thread() {
  for (int i = 0; i < MAX_THREADS; ++i) {
    if (system_threads[i].free == TRUE) return &system_threads[i];
  }
  return NULL;
}

int pthread_create(pthread_t *thread, int priority, void *(*start_function) (void *), void *start_arg) {
  thread->free = FALSE;
  thread->start_function = start_function;
  thread->start_arg = start_arg;
  thread->state = THR_STATE_READY;
  return 0;
}

pthread_t *pthread_self() {
  return &system_threads[current_thread_index];
}

pthread_t *thread_with_max_priority() {
  pthread_t *max_prio_thread = NULL;
  for (int i = 0; i < MAX_THREADS; ++i) {
    if (system_threads[i].free == TRUE) continue;
    if (max_prio_thread && system_threads[i].sched_priority > max_prio_thread->sched_priority) {
      max_prio_thread = &system_threads[i];
    }
  }

  return max_prio_thread;
}

void pthread_schedule() {
  printf("Running scheduler\n");
  pthread_t *current_thread = pthread_self();
  pthread_t *max_prio_thread = thread_with_max_priority();
  if (current_thread == max_prio_thread) return;

}

void pthread_scheduler_init() {
  timer_create_alarm((timespec_t){0, 1e8}, CLOCK_MONOTONIC, pthread_schedule);
}
