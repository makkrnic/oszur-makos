#pragma once

#include <arch/types.h>

#define I8253_FREQ  1193180
#define I8253_PORT  0x40 // CH0 data port
#define I8253_CMD   0x43 // CMD register, write only
#define I8253_LOAD_CMD  0x34 // CH0, lo/hibyte, rate generator, 16bit binary
#define I8253_LATCH_CMD 0x04

#define TIMER_COUNT_MAX 0xFFFF
#define TIMER_COUNT_MIN ( TIMER_COUNT_MAX >> 10 )

#define GIGA 1e9l

#define ALARMS_COUNT 20

typedef long time_t;

typedef struct {
  time_t sec;
  long   nsec;
} timespec_t;

#define TICK_TO_TIME(TICK, TIME) \
  do { \
    (TIME)->sec = 0; \
    (TIME)->nsec = (TICK * GIGA) / I8253_FREQ; \
  } while (0);

#define TIME_TO_TICK(TIME, TICK) \
  do { \
    (TICK = (TIME)->nsec * GIGA / I8253_FREQ; \
  } while (0);

typedef enum {
  CLOCK_MONOTONIC,
  CLOCK_REALTIME
} clock_type_t;

typedef void(*alarm_activation)();

typedef struct _periodic_alarm_t {
  u8 enabled;
  timespec_t period;
  timespec_t next_activation;
  clock_type_t clock_type;
  void (*activation)(struct _periodic_alarm_t*);
  void (*destroy)();
} periodic_alarm_t;

void timer_init();
void timer_set_cnt(u16 cnt);
u16  timer_get_cnt();
void timer_enable_interrupt();
void timer_disable_interrupt();

timespec_t timer_get_time(clock_type_t type);
void timer_set_time(timespec_t *new_time);
void timer_register_interrupt(void *handler);

void timer_sleep(timespec_t delay);
void timer_sleep_until(timespec_t time);

periodic_alarm_t *timer_create_alarm(timespec_t period, clock_type_t clock_type, alarm_activation activation);
void timer_destroy_alarm(periodic_alarm_t *alarm);
