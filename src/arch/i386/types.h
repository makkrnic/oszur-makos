#pragma once

typedef unsigned char   arch_u8;
typedef unsigned short  arch_u16;
typedef unsigned int    arch_u32;
