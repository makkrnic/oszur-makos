#pragma once

#include <arch/types.h>

#define INIT_EFLAGS     0x0202

#define MAX_THREADS     32
#define THREAD_STACK_SIZE 0x100

typedef struct _arch_context_t_
{
   u32    edi, esi, ebp, _esp, ebx, edx, ecx, eax;
   u32   eflags;
   u32   eip;
} __attribute__((__packed__)) context_t;

typedef void (*thread_func)(void *);

void arch_create_thread_context (
    context_t **context,
    thread_func func,
    void *param,
    void (*thread_exit)(),
    void *stack,
    size_t stack_size
);

void arch_switch_to_thread (context_t **from, context_t **to);


typedef enum {
  THR_STATE_ACTIVE = 1,
  THR_STATE_READY,
  THR_STATE_WAIT,
  THR_STATE_SUSPENDED,
  THR_STATE_PASSIVE
} pthread_state_t;

typedef struct _pthread_t {
  unsigned char free;
  int id;

  void *(*start_function)(void *);
  void *start_arg;
  pthread_state_t state;
  void *stack_address;
  int sched_priority;

  context_t *context;
} pthread_t;

pthread_t *pthread_get_free_thread();
void pthreads_init();
int pthread_create(pthread_t *thread, int priority, void *(*start_function) (void *), void *start_arg);
pthread_t *pthread_self();

void pthread_scheduler_init();
