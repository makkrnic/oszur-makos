#include "serial.h"
#include "io.h"

#define SERIAL_ADDR   0x03f8

/* http://www.lookrs232.com/rs232/dlab.htm */
/* http://wiki.osdev.org/Serial_Ports */
#define DLAB          0x80

#define TX            0
#define RX            0
#define IE            1 /* Interrupt enable */
#define LC            3 /* Line control     */
#define LS            5 /* Line status      */

#define DLL           0 /* Divisor latch low (with DLAB = 1) */
#define DLH           1 /* Divisor latch low (with DLAB = 1) */

#define BAUD          9600
#define BAUD_DIVISOR_L 0x0C
#define BAUD_DIVISOR_H 0x00
#define SERIAL_OPTIONS 0x03 /* 8 bits, 1 stop bit, no parity */

void k_serial_init() {
  u16 port = SERIAL_ADDR;


  outb(0, port + IE);
  /* set divisor */
  u8 lcr = DLAB;
  outb(lcr, port + LC);
  outb(BAUD_DIVISOR_L, port + DLL);
  outb(BAUD_DIVISOR_H, port + DLH);

  /* unset DLAB and configure other options */
  outb(SERIAL_OPTIONS, port + LC);
}

static inline u8 is_transmitter_empty() {
  return inb(SERIAL_ADDR + LS) & 0x20;
}

void k_serial_putchar(u8 c) {
  while (is_transmitter_empty() == 0);
  outb(c, SERIAL_ADDR);
}
