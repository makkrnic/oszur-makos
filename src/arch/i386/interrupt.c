#include "interrupt.h"
#include "io.h"

#include<api/stdio.h>

static void (*i_handlers[INTERRUPTS_NUMBER]) (unsigned int);

#define PIC1_CMD  0x20 // master PIC command
#define PIC1_DATA 0x21 // master PIC data

#define PIC2_CMD  0xA0 // slave PIC command
#define PIC2_DATA 0xA1 // slave PIC data

#define PIC_EOI   0x20 // PIC End of interrupt command

static void ic_init() {
  outb(0x11, PIC1_CMD);
  outb(0x11, PIC2_CMD);

  outb(IRQ_OFFSET & 0xF8, PIC1_DATA);
  outb((IRQ_OFFSET + 8) & 0xF8, PIC1_DATA);

  outb(0x04, PIC1_DATA);
  outb(0x02, PIC2_DATA);

  outb(0x01, PIC1_DATA);
  outb(0x01, PIC2_DATA);

  outb(0xFB, PIC1_DATA);
  outb(0xFF, PIC2_DATA);

  outb(0, PIC1_DATA);
}

//*
static void ic_irq_enable(unsigned int irq) {
  irq -= IRQ_OFFSET;

  printf("Enabling interrupt %d\n", irq);
  if (irq < 8) {
    outb(inb(PIC1_DATA) & ~(1 << irq), PIC1_DATA);
  }
  else {
    outb(inb(PIC2_DATA) & ~(1 << (irq - 8)), PIC2_DATA);
  }
}
// */
void arch_enable_interrupt(u32 irq) {
  ic_irq_enable(irq);
}

//*
static void ic_irq_disable(unsigned int irq) {
  irq -= IRQ_OFFSET;

  if (irq < 8) {
    outb(inb(PIC1_DATA) | (1 << irq), PIC1_DATA);
  }
  else {
    outb(inb(PIC2_DATA) | (1 << (irq - 8)), PIC2_DATA);
  }
}
// */
void arch_disable_interrupt(u32 irq) {
  ic_irq_disable(irq);
}

void ic_at_exit(u32 irq) {
  if (irq >= IRQ_OFFSET && irq < HW_INTERRUPTS) {
    outb(PIC_EOI, PIC1_CMD);
    if (irq >= IRQ_OFFSET + 8) {
      outb(PIC_EOI, PIC2_CMD);
    }
  }
}

void arch_init_interrupts() {
  printf("Initializing interrupts: start\n");
  ic_init();

  asm volatile ( "sti\n\t" );
  printf("Initializing interrupts: finish\n");
}

void arch_interrupt_handler(unsigned int irq) {
  //printf("Interrupt handler");
  if (i_handlers[irq]) {
    i_handlers[irq](irq);
  }

  ic_at_exit(irq);
}

void arch_register_interrupt_handler(u32 int_num, void *handler) {
  i_handlers[int_num] = handler;
  ic_irq_enable(int_num);
}
