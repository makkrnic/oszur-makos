#pragma once

#define INTERRUPTS_NUMBER 49

#define IRQ_OFFSET 0x20

enum {
  IRQ_TIMER = IRQ_OFFSET,
  HW_INTERRUPTS
};


#include <arch/interrupt.h>

