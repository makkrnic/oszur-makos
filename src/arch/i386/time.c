#include "time.h"
#include "io.h"
#include "interrupt.h"

#include <stdio.h>
#include <ARCH/processor.h>

timespec_t clock_monotonic = { 0, 0 };
timespec_t clock_realtime = { 0, 0 };

void timer_underflow(u32);
void (*timer_ihandler)(u32) = NULL;
volatile char continue_sleeping = FALSE;
volatile char continue_sleeping_until = FALSE;
static timespec_t sleep_delay;
static timespec_t sleep_until;

periodic_alarm_t alarms[ALARMS_COUNT] = { { 0 } };

void timer_init() {
  timer_set_cnt(TIMER_COUNT_MAX);

  arch_register_interrupt_handler(IRQ_TIMER, timer_underflow);
}

void timer_set_cnt(u16 cnt) {
  outb(I8253_LOAD_CMD, I8253_CMD);
  outb(cnt & 0xff, I8253_PORT);
  outb((cnt >> 8) & 0xff, I8253_PORT);
}

u16 timer_get_cnt() {
  u8 low, high;
  outb(I8253_LATCH_CMD, I8253_CMD);
  low = inb(I8253_PORT);
  high = inb(I8253_PORT);

  return low + (high << 8);
}

void timer_enable_interrupt() {
  printf("Enabling timer irq: %d", IRQ_TIMER);
  arch_enable_interrupt(IRQ_TIMER);
}

void timer_disable_interrupt() {
  arch_disable_interrupt(IRQ_TIMER);
}


timespec_t timer_get_time(clock_type_t type) {
  if (type == CLOCK_MONOTONIC) return clock_monotonic;
  return clock_realtime;
}

void timer_set_time(timespec_t *time) {
  clock_realtime = *time;
}

void time_add(timespec_t *dst, const timespec_t *src) {
  dst->sec += src->sec;
  dst->nsec += src->nsec;

  if (dst->nsec > GIGA) {
    dst->sec++;
    dst->nsec -= GIGA;
  }
}

void time_sub(timespec_t *dst, const timespec_t *src) {
  dst->sec -= src->sec;
  dst->nsec -= src->nsec;

  if (dst->nsec < 0) {
    dst->sec--;
    dst->nsec += GIGA;
  }
}

char is_time_positive(timespec_t* time) {
  return (time->sec > 0 && time->nsec > 0) || (time->sec == 0 && time->nsec > 0);
}

void timer_alarm_activate(periodic_alarm_t *alarm) {
  timespec_t current_time = timer_get_time(alarm->clock_type);
  timespec_t next_activation = current_time;
  time_add(&next_activation, &alarm->period);
  alarm->next_activation = next_activation;

  if (alarm->activation) alarm->activation(alarm);
}

void timer_underflow(u32 irq) {
  timespec_t to_add;
  TICK_TO_TIME(TIMER_COUNT_MAX, &to_add);

  if (continue_sleeping) {
    time_sub(&sleep_delay, &to_add);
    if (!is_time_positive(&sleep_delay)) {
      continue_sleeping = FALSE;
    }
  }

  time_add(&clock_realtime, &to_add);
  time_add(&clock_monotonic, &to_add);

  if (continue_sleeping_until) {
    timespec_t sleep_until_copy = sleep_until;
    timespec_t current_time = timer_get_time(CLOCK_REALTIME);
    time_sub(&sleep_until_copy, &current_time);
    if (!is_time_positive(&sleep_until_copy)) {
      continue_sleeping_until = FALSE;
    }
  }

  timespec_t current_time_m = timer_get_time(CLOCK_MONOTONIC);
  timespec_t current_time_r = timer_get_time(CLOCK_REALTIME);

  for (int i = 0; i < ALARMS_COUNT; ++i) {
    if (!alarms[i].enabled) continue;

    timespec_t current_time = alarms[i].clock_type == CLOCK_MONOTONIC ? current_time_m : current_time_r;
    timespec_t next_activation = alarms[i].next_activation;
    time_sub(&next_activation, &current_time);

    if (is_time_positive(&next_activation)) continue;
    timer_alarm_activate(&alarms[i]);
  }

  if (timer_ihandler) {
    timer_ihandler(IRQ_TIMER);
  }
}

void timer_register_interrupt(void *handler) {
  timer_ihandler = handler;
}

void timer_sleep(timespec_t delay) {
  int interrupts = set_interrupts(FALSE); // save current state

  sleep_delay = delay;
  timespec_t wait;
  int remaining_ticks = timer_get_cnt();
  TICK_TO_TIME(remaining_ticks, &wait);
  time_add(&sleep_delay, &wait);

  continue_sleeping = TRUE;
  do {
    enable_interrupts();
    suspend();
    disable_interrupts();
  } while (continue_sleeping);

  set_interrupts(interrupts); // load saved state
}

void timer_sleep_until(timespec_t time) {
  int interrupts = set_interrupts(FALSE); // save current state

  sleep_until = time;
  continue_sleeping_until = TRUE;
  do {
    enable_interrupts();
    suspend();
    disable_interrupts();
  } while (continue_sleeping_until);

  set_interrupts(interrupts); // load saved state
}

periodic_alarm_t *timer_create_alarm(timespec_t period, clock_type_t clock_type, alarm_activation activation) {
  periodic_alarm_t *alarm = NULL;
  for (int i = 0; i < ALARMS_COUNT; ++i) {
    if (!alarms[i].enabled) {
      alarm = &alarms[i];
      break;
    }
  }
  if (alarm == NULL) return NULL;

  alarm->period = period;
  alarm->clock_type = clock_type;
  alarm->activation = activation;
  alarm->next_activation = timer_get_time(clock_type);
  alarm->enabled = TRUE;

  return alarm;
}

void timer_destroy_alarm(periodic_alarm_t *alarm) {
  alarm->enabled = FALSE;
  alarm->period = (timespec_t) { 0, 0 };
  alarm->next_activation = (timespec_t) { 0, 0 };
  alarm->activation = NULL;
}
