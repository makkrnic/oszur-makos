#include "io.h"

void outb(u8 data, u16 port) {
  asm volatile("outb %0, %1" : : "a" (data), "dN" (port));
}

u8 inb(u16 port) {
  u8 data;
  asm volatile("inb %1, %0" : "=a" (data) : "dN" (port));
  return data;
}
