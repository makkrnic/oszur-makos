#pragma once

#include <arch/types.h>

void k_serial_init();
void k_serial_putchar(u8 c);
