#pragma once

#include <arch/types.h>

void outb(u8 data, u16 port);
u8 inb(u16 port);
