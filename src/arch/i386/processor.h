#pragma once

#define enable_interrupts() asm volatile("sti\n\t")
#define disable_interrupts() asm volatile("cli\n\t")
#define suspend() asm volatile("hlt\n\t")

#define EFLAGS_IF        0x00000200      /* Interrupt Enable     (9) */

/*! Enable or disable interrupts and return previous state */
static inline int set_interrupts ( int enable )
{
   int old_flags;

   asm volatile (  "pushf\n\t"
         "pop    %0\n\t"
         : "=r" (old_flags) );
   if (enable)
      enable_interrupts ();
   else
      disable_interrupts ();

   return old_flags & EFLAGS_IF;
}
