#include <api/stdio.h>

#include <stdarg.h>

#include <ARCH/serial.h>

void putchar(char c) {
  k_serial_putchar(c);
  if (c == '\n')
    k_serial_putchar('\r');
}

int puts_inline(const char *s) {
  int i = 0;
  while(s[i]) {
    putchar(s[i]);
    ++i;
  }
  return i;
}

int puts(const char* s) {
  int i = puts_inline(s);
  putchar('\n');
  return i+1;
}

int printf(const char* format, ...) {
  int characters_put = 0;
  int i = 0;

  va_list args;
  va_start(args, format);

  while(format[i]) {
    if (format[i] == '%') {
      // read format specifier
      ++i;

      // or escape character
      if (format[i] == '%') {
        putchar('%');
        ++characters_put;
      }
      else {
        switch(format[i]) {
          case 's':
          {
            char* s = va_arg(args, char*);
            characters_put += puts_inline(s);
            break;
          }
          case 'c':
          {
            char c = va_arg(args, int);
            putchar(c);
            ++characters_put;
            break;
          }
          case 'd':
          {
            int d = va_arg(args, int);
            char str[10] = {0};
            int j = 0;
            unsigned long divisor = 1e9;
            char leading = 0;

            while (j < 10 && divisor > 0) {
              char c = d/divisor;
              d %= divisor;
              divisor /= 10;

              if (c != 0 || leading || !divisor) {
                leading = 1;
                str[j] = c + '0';
                j++;
              }
            }

            characters_put += puts_inline(str);

            break;
          }
          case 'l':
          {
            // only %ld is supported so move to the next character
            ++i;
            long d = va_arg(args, long);
            char str[10] = {0};
            int j = 0;
            unsigned long divisor = 1e9;
            char leading = 0;

            while (j < 10 && divisor > 0) {
              char c = d/divisor;
              d %= divisor;
              divisor /= 10;

              if (c != 0 || leading || !divisor) {
                leading = 1;
                str[j] = c + '0';
                j++;
              }
            }

            characters_put += puts_inline(str);

            break;
          }
        }
      }
    }
    else {
      putchar(format[i]);
      ++characters_put;
    }
    ++i;
  }

  va_end(args);
  return characters_put;
}
