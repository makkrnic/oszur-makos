//#include<ARCH/serial.h>
#include<api/stdio.h>
#include<arch/interrupt.h>
#include<ARCH/time.h>
#include<arch/types.h>

#include<ARCH/thread.h>

void ihandler(u32 irq) {
  //static u8 i = ~0;

  //if (i > 63) {
  //  i = 0;
  //  printf("Interrupt handler %d.\n", irq);
  //}
  //++i;
  //if (irq == IRQ_TIMER) {
  //  timer_set_cnt(0xF);
  //}
  
  timespec_t t = timer_get_time(CLOCK_REALTIME);
  printf("Time: %lds, %ldns\n", t.sec, t.nsec);
}

void periodic_write_2(periodic_alarm_t *alarm) {
  static int remaining = 10;
  printf("Periodic 2. Remaining: %d\n", remaining);
  remaining--;
  if (remaining < 0) {
    timer_destroy_alarm(alarm);
  }
}

void periodic_write_4(periodic_alarm_t *alarm) {
  static int remaining = 10;
  printf("Periodic 4. Remaining: %d\n", remaining);
  remaining--;
  if (remaining < 0) {
    timer_destroy_alarm(alarm);
  }
}

void *test_thread(void *arg) {
  int i = *((int *)arg);
  printf("Test thread: %d\n", i);
  return 0;
}

void *idle_thread(void *arg) {
  while(1);
}

void kmain() {
  //k_serial_init();

  //puts("Testiram puts\n");
  //printf("Testiram printf.\n%%abc");
  //printf("Testiram stringove - string: %s\n", "abcde");
  //printf("a\n");
  //printf("1 = %d", 1);
  //printf("Character: %c\n", 'E');

  arch_init_interrupts();
  for (int i = 32; i < 47; ++i) {
    if (i == IRQ_TIMER) continue;
    arch_register_interrupt_handler(i, ihandler);
  }
  timer_init();
  //timer_register_interrupt(ihandler);
  //timer_enable_interrupt();
  //timer_disable_interrupt();
  
  printf("%d, %d\n", 361760165, 361760165);

  printf("Timer.0: %d\n", timer_get_cnt());
  printf("Timer.1: %d\n", timer_get_cnt());

  printf("Setting time to 25s\n");
  timespec_t new_time = { 25, 0 };
  timer_set_time(&new_time);
  new_time = timer_get_time(CLOCK_REALTIME);
  printf("Current time is %lds %ldns\n", new_time.sec, new_time.nsec);

  //timer_create_alarm((timespec_t){2, 0}, CLOCK_MONOTONIC, periodic_write_2);
  //timer_create_alarm((timespec_t){4, 0}, CLOCK_MONOTONIC, periodic_write_4);

  ////for (long long i = 0; i < 4e7; ++i);
  ////new_time = timer_get_time(CLOCK_REALTIME);
  ////printf("Current time is %lds %ldns\n", new_time.sec, new_time.nsec);

  //for (int i = 0; i < 10; ++i) {
  //  printf("sleeping: %d\n", i);
  //  timer_sleep((timespec_t) { 1, 0 });
  //}

  //printf("Sleeping until 40, 0\n");
  //timer_sleep_until((timespec_t) { 40, 0 });
  //new_time = timer_get_time(CLOCK_REALTIME);
  //printf("Current time is %lds %ldns\n", new_time.sec, new_time.nsec);


  pthreads_init();

  pthread_t *thr = pthread_get_free_thread();
  pthread_create(thr, 0, idle_thread, NULL);

  pthread_scheduler_init();

  while(1) {
  }
}
