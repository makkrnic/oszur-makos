PROJECT=makos

ARCH=i386

CC=gcc
LD=ld

CFLAGS=-std=gnu99 -m32 -Wall -Werror -ffreestanding -nostdlib -fno-stack-protector -g
LDFLAGS=-melf_i386 -e arch_start -Ttext=0x100000

SOURCEDIR:=src
BUILDDIR:=build

LDSCRIPT = $(BUILDDIR)/ARCH/boot/ldscript.ld

DIRS := arch/$(ARCH)/boot arch/$(ARCH) kernel api

SOURCES := $(foreach DIR,$(DIRS),$(wildcard $(SOURCEDIR)/$(DIR)/*.c $(SOURCEDIR)/$(DIR)/*.S))
SOURCES := $(SOURCES:$(SOURCEDIR)/%=%)
OBJECTS := $(addprefix $(BUILDDIR)/,$(SOURCES:.c=.o))
OBJECTS := $(OBJECTS:.S=.asm.o)
DEPS := $(OBJECTS:.o=.d)

INCLUDES := $(SOURCEDIR)/include $(BUILDDIR)

DIRS_CREATED := $(BUILDDIR)/.null

LOAD_ADDR = 0x100000

CMACROS += ARCH="\"$(ARCH)\"" LOAD_ADDR=$(LOAD_ADDR)

all: build

dir:
	mkdir -p $(BUILDDIR)

build: dir link

$(DIRS_CREATED):
	@if [ ! -e $(BUILDDIR) ]; then mkdir -p $(BUILDDIR); fi;
	@$(foreach DIR,$(DIRS), if [ ! -e $(BUILDDIR)/$(DIR) ];	\
		then mkdir -p $(BUILDDIR)/$(DIR); fi; )
	ln -s ../$(SOURCEDIR)/arch/$(ARCH) $(BUILDDIR)/ARCH
	touch $(DIRS_CREATED)

$(BUILDDIR)/%.o: $(SOURCEDIR)/%.c $(DIRS_CREATED)
	@echo compiling $<
	@$(CC) -c $< -o $@ -MMD $(CFLAGS)       \
	  $(foreach INC,$(INCLUDES),-I$(INC))  \
	  $(foreach MACRO,$(CMACROS),-D $(MACRO))


$(BUILDDIR)/%.asm.o: $(SOURCEDIR)/%.S $(DIRS_CREATED)
	@echo compiling $<
	@$(CC) -c $< -o $@ -MMD $(CFLAGS)       \
	  $(foreach INC,$(INCLUDES),-I$(INC))  \
	  $(foreach MACRO,$(CMACROS),-D $(MACRO))

LDSCRIPT_PP := $(BUILDDIR)/ldscript.ld
$(LDSCRIPT_PP): $(LDSCRIPT) $(DIRS_CREATED)
	@$(CC) -E -P -x c -o $@ $< $(CFLAGS)    \
	  $(foreach INC,$(INCLUDES),-I$(INC))  \
	  $(foreach MACRO,$(CMACROS),-D $(MACRO))

link: $(OBJECTS) $(LDSCRIPT_PP)
	@echo linking
	@$(LD) $(OBJECTS) -o $(BUILDDIR)/$(PROJECT)  -T $(LDSCRIPT_PP) $(LDFLAGS)

run:

clean:
	-rm $(OBJECTS)
	-rm $(DEPS)
	-rm $(BUILDDIR)/$(PROJECT)

-include $(DEPS)
